from django.db import models

class Note(models.Model):
    untuk = models.CharField(max_length=30)
    dari = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    message = models.CharField(max_length=30)