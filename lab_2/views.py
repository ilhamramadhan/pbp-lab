from django.shortcuts import render
from django.http.response import HttpResponse
from lab_2.models import Note
from django.core import serializers

def index(request):
    notes = Note.objects.all()
    response = { 'notes' : notes }
    return render(request, 'index_lab2.html', response)

def xml(request):
    notes = Note.objects.all()
    xmlNotes = serializers.serialize('xml', notes)
    return HttpResponse(xmlNotes, content_type = "application/xml")

def json(request):
    notes = Note.objects.all()
    jsonNotes = serializers.serialize('json', notes)
    return HttpResponse(jsonNotes, content_type = "application/json")