from django.shortcuts import redirect, render
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.contrib.auth.decorators import login_required

@login_required(login_url = "/admin/login/")

def index(request):
    friends = Friend.objects.all().values()
    response = {'teman': friends}
    return render(request, 'friend_list_lab1.html', response)

@login_required(login_url = "/admin/login/")

def add_friend(request):
    context ={}
  
    form = FriendForm(request.POST or None, request.FILES or None)
      
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect("/lab-3")
  
    context['form']= form
    return render(request, "lab3_form.html", context)

