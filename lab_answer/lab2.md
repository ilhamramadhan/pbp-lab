Jawaban nomor 1:
- dari segi bahasa, XML merupakan sebuah markup language (lebih tepatnya adalah SGML / Standard Generalized Markup Language), sementara JSON memiliki dasar bahasa JavaScript
- dari segi cara penyimpanan data, XML menyimpan data dalam bentuk tag-tag berdasarkan obyek yang dibuat, sementara JSON menyimpan data dalam bentuk mapping (menggunakan key dan value)

Sumber: 
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://www.guru99.com/json-vs-xml-difference.html


Jawaban nomor 2:
- XML memiliki tujuan untuk mentransfer data sedangkan HTML lebih ditujukan untuk penyajian data.
- XML Case Sensitive sementara HTML tidak.

Sumber: https://blogs.masterweb.com/perbedaan-xml-dan-html/